// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// set Timeout - дозволяє викликати функцію один раз через, викликаний нами, інтервал часу. SetInterval - дозволяє викликати функцію регулярно, повторючи її виклик через, заданий нами, інтервал часу

// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Навіть якщо передати в функцію нульову затримку, вона не виконається миттєво. Вона буде додана в чергу виконання і виконається, коли буде готовий стек виконання JS

// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Якщо забути викликати функцію clearInterval() - це може викликати деякі проблеми. Наприклад: Витрата ресурсів, замороження сторінки, неналежне функціювання






const stop = document.querySelector('#stop');
const start = document.querySelector('#start');


let slideI = 0;
showSlides();

let interval = setInterval(showSlides,3000);

stop.addEventListener('click', () => {
    clearInterval(interval);
    start.removeAttribute('disabled');
    stop.setAttribute('disabled', true);

});

start.addEventListener('click', () => {
    setInterval(showSlides, 3000);
    start.setAttribute('disabled',true);
    stop.removeAttribute('disabled');

});

function showSlides() {
    let i;
    const slides = document.getElementsByClassName('image-to-show');

    for (i=0; i < slides.length; i++){
        slides[i].classList.add('hidden');
    }
    slideI++;
    if (slideI > slides.length) slideI = 1;
    slides[slideI - 1].classList.remove('hidden')

}
